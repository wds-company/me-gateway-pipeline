#!/bin/bash
imgname="tb-me-gateway"
version="1.0.4"
dockerRegistry="portusdev.beebuddy.net"
  #echo "Fi\'iy[0eoe" | docker login --username imgadmin --password-stdin wdsregistry1.beebuddy.net:9900
  docker tag $imgname:$version $dockerRegistry/$imgname:$version
  docker push $dockerRegistry/$imgname:$version
  docker rmi $dockerRegistry/$imgname:$version
