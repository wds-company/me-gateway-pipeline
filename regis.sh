#!/bin/bash
imgname="tb-me-gateway"
version="v1.3.1"
  #echo "Fi\'iy[0eoe" | docker login --username imgadmin --password-stdin wdsregistry1.beebuddy.net:9900
  docker tag $imgname:$version wdsregistry1.beebuddy.net:9900/$imgname:$version
  docker push wdsregistry1.beebuddy.net:9900/$imgname:$version
  docker rmi wdsregistry1.beebuddy.net:9900/$imgname:$version
